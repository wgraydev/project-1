<?php
error_reporting(E_ALL);
/*************************
* Database Connections
*************************/
$link = new mysqli("localhost","wlg11a","landend","project1db");

if ($link->connect_errno) {
    exit();
}

function getUsernameByEmail($mail)
{
  $link = new mysqli("localhost","wlg11a","landend","project1db");
  $result = $link->query("SELECT * FROM users WHERE email='". $mail ."' limit 1");
      if(!$result)
        die ('Can\'t query users because: ' . $link->error);
      else
      {
        while($row = $result->fetch_assoc()):
          $result->close();
          return $row["username"];
        endwhile;
      }
}

/**************************
* Database interactions
***************************/
$message = "";
$action = isset($_REQUEST["action"]) ? $_REQUEST['action'] : 'none';

if($action == "signup")
{
  $username = isset($_POST["username"]) ? $_POST["username"] : '';
  $fname    = isset($_POST["fname"]) ? $_POST["fname"] : '';
  $lname    = isset($_POST["lname"]) ? $_POST["lname"] : '';
  $email    = isset($_POST["email"]) ? $_POST["email"] : '';
  $password = isset($_POST["password"]) ? $_POST["password"] : '';


  $result = $link->query("INSERT INTO `users` (`username`, `firstname`, `lastname`, `email`, `password`, `signup_date`, `is_moderator`) VALUES ('". $username ."','". $fname ." ','". $lname ."','" . $email ."','". $password ."',Now(),0)");

  if(!$result)
    die ('Can\'t query users because: ' . $link->error);
  else
    $message = "Signup Complete-----";
}
elseif($action == "login")
{
  $email = $_POST["email"];
  $password    = $_POST["password"];

  $link = new mysqli("localhost","wlg11a","landend","project1db");
  $result = $link->query("SELECT * FROM `users` WHERE  email='". $email. "' and password ='". $password ."'");

  if(!$result)
    die ('Can\'t query users because: ' . $link->error);
  else
  {
    $message = "login Complete";

  }
}
elseif($action == "submitpost")
{
	$title    = isset($_POST["title"]) ? $_POST["title"] : 'none';
	$content  = isset($_POST["content"]) ? $_POST["content"] : '';
	$email    = isset($_POST["email"]) ? $_POST["email"] : '';
	$username = 'getUsernameByEmail';
	/*echo '<script type="text/javascript">alert("'. $username($email) .'"); </script>';*/

  	$result = $link->query("INSERT INTO `posts` (`title`, `content`, `submitted_by`, `submission_date`) VALUES ('". $title. "','". $content ."','". $username($email) ."', Now())");

  	if(!$result)
    	die ('Can\'t query posts because: ' . $link->error);
  	else
  	{
    	$message = "Post submitted";
  	}
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>I <3 News</title>
		<script src="scripts/home.js"></script>
		<script>window.onload = pageLoad;</script>
		<link rel="stylesheet" href="styles/home.css" />
		
		<meta charset="utf-8">
		<meta property="og:url" content="homepage.html" />
		<meta property="og:title" content="Home" />
		<meta property="og:description" content="This is a static blog." />
		<meta property="og:image" content="" />
	</head>
	
	<body>
		<header class="bar" role="banner">
			<div>
				<h1 id="site-title">I <3 News</h1>
			</div>
			<div role="search">
				<form id="search-field">
					<input type="text" placeholder="search" style="">
				</form>
			</div>
			<div>
				<div id="guest-buttons">
					<button onclick="signIn()">Sign in</button>
					<button onclick="signUp()">Sign up</button>
				</div>
				<div id="user-buttons">
					<div id="user-name">Logged in as: </div>
					<button onclick="signOut()">Sign out</button>
				</div>
			</div>
		</header>
		
		<main id="content">
			<article id="posts" role="main">
				<div id="submit-post">
					<form id="post-fields" onkeyup="validatePost()" action="" method="post">
						<input type="hidden" name="action" value="submitpost" />
						<input type="hidden" name="email" value="" />
						<input type="text" placeholder="Title" style="" name="title" />
						<textarea placeholder="Type your post here." rows="10" cols="50" name="content"></textarea>
						<input type="submit" value="Get Approved" onclick="submitPost()" disabled="disabled" />
					</form>
				</div>
				
				<?php
					$link = new mysqli("localhost","wlg11a","landend","project1db");
					$result = $link->query("SELECT * FROM posts ORDER BY submission_date DESC");
					while($row = $result->fetch_assoc()):?>
						<h1 class="post-title">
							<?php print $row["title"];?>
						</h1>
						<h4>
							Submitted by <?php print $row["submitted_by"];?> on <?php print $row["submission_date"];?>. Approved by Moderator.
						</h4>
						<p>
							<?php print $row["content"];?>
						</p>
					<?php endwhile;
					$result->close();
				?>
				
				<h1 class="post-title">
					Butter
				</h1>
				<h4>
					Submitted by Example on 12/12/12. Approved by Moderator.
				</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lobortis elit metus, a tristique nisi congue nec. Duis cursus, purus ut fermentum vestibulum, ante massa ultrices orci, eu varius turpis odio sit amet ligula. Donec ut tellus eget elit aliquet luctus id et tortor. Proin commodo elit ac odio facilisis, nec luctus est interdum. In et ante rhoncus, faucibus ipsum a, elementum enim. Sed mollis diam ac convallis varius. Quisque mauris tortor, interdum quis condimentum nec, euismod at lacus. Nulla quis enim elit. Duis a est non dolor consequat bibendum.
				</p>
			</article>

			<div id="sidebar" role="sidebar">
				<nav>
					<a href="">Toast Related Posts</a>
					<br/>
					<a href="">Other Posts</a>
					<br/>
					<a href="">Articles</a>
					<br/>
					<a href="">Toasters</a>
				</nav>
			</div>
		</main>
		
		<div id="sign-in">
			<div>
				<form id="sign-in-fields" onkeyup="validateSignIn()" action="" method="post">
					<input type="hidden" name="action" value="login" />
					<input type="email" class="email" placeholder="Email" name="email" />
					<input type="password" placeholder="Password" name="password" />
					<input type="submit" onclick="submitSignIn()" disabled="disabled" />
					<input type="button" value="Cancel" onclick="cancelSignIn()" />
				</form>
				<form id="sign-up-fields" onkeyup="validateSignUp()" action="" method="post">
					<input type="hidden" name="action" value="signup" />
					<input type="text" placeholder="First name" name="fname" />
					<input type="text" placeholder="Last name" name="lname" />
					<input type="email" class="email" placeholder="Email" name="email" />
					<input type="password" placeholder="Password" name="password" />
					<input type="password" placeholder="Re-enter Password" />
					<input type="submit" onclick="submitSignIn()" disabled="disabled" />
					<input type="button" value="Cancel" onclick="cancelSignIn()" />
				</form>
			</div>
		</div>
		
		<footer>
			<nav class="bar">
				<div>
					<a href="">Home</a>
				</div>
				<div>
					<a href="">Blogs</a>
				</div>
				<div>
					<a href="">News</a>
				</div>
			</nav>
		</footer>
	</body>
</html>