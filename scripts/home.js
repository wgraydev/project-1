function pageLoad()
{
	if(sessionStorage.getItem('logged-in') === 'true')
	{
		var email = sessionStorage.getItem('email');
		if(email)
		{
			var user = document.getElementById('user-name');
			user.textContent = user.textContent + email;
			
			document.getElementById('submit-post').style.display = 'block';
			document.getElementById('post-fields')[1].value = email;
			
			document.getElementById('guest-buttons').style.display='none';
			document.getElementById('user-buttons').style.display='flex';
		}	
		else
		{
			sessionStorage.setItem('logged-in', 'false');
		}
	}
}

function signIn()
{
	document.getElementById('sign-in').style.display = 'flex';
	var fields = document.getElementById('sign-in-fields');
	fields.style.display = 'flex';
	fields[0].focus();
}

function submitSignIn()
{
	var data = document.getElementById('sign-in');
	data.style.display = 'none';
	
	document.getElementById('guest-buttons').style.display='none';
	document.getElementById('user-buttons').style.display='flex';
	
	var email = data.getElementsByClassName('email');
	sessionStorage.setItem('email', email[0].value || email[1].value);
	sessionStorage.setItem('logged-in', 'true');
	
	document.getElementById('submit-post').style.display = 'block';
}

function signOut()
{
	document.getElementById('user-buttons').style.display='none';
	document.getElementById('guest-buttons').style.display='flex';
	
	sessionStorage.setItem('logged-in', 'false');
	document.getElementById('submit-post').style.display = 'none';
}

function cancelSignIn()
{
	document.getElementById('sign-in').style.display = 'none';
	document.getElementById('sign-in-fields').style.display = 'none';
	document.getElementById('sign-up-fields').style.display = 'none';
}

function signUp()
{
	document.getElementById('sign-in').style.display = 'flex';
	var fields = document.getElementById('sign-up-fields');
	fields.style.display = 'flex';
	fields[0].focus();
}

function submitPost()
{
	var data = document.getElementById('post-fields')
	var errorAlert = 0;
	
	for (var i=2; i<4; i++)
		if(!data[i].value)
			errorAlert = 1;
	
	if(errorAlert === 1)
	{
		alert('Error in validation');
		return;
	}
	
	alert('Post sumbitted for approval.')
}

function validatePost()
{
	var data = document.getElementById('post-fields');
	
	var valid = 1;
	for (var i=2; i<4; i++)
		if(!data[i].value)
		{
			valid = 0;
			break;
		}
	
	data[4].disabled = !valid;
}

function validateSignIn()
{
	var data = document.getElementById('sign-in-fields');
	
	var valid = 1;
	for (var i=1; i<3; i++)
		if(!data[i].value)
		{
			valid = 0;
			break;
		}
	
	data[3].disabled = !valid || !data[1].validity.valid;
}

function validateSignUp()
{
	var data = document.getElementById('sign-up-fields');
	
	var valid = 1;
	for (var i=1; i<6; i++)
		if(!data[i].value)
		{
			valid = 0;
			break;
		}
	
	data[6].disabled = !valid || data[4].value !== data[5].value || !data[3].validity.valid;
}
